from django.shortcuts import redirect, render, get_object_or_404
import stripe
from datetime import datetime, timedelta
import datetime
from django.core.mail import send_mail
from django.conf import settings
from django.shortcuts import get_object_or_404
from tours.models import TourDetail, Tour
from tours.views import single_tour
from .forms import BillingForm
from checkout.models import Billing, Voucher, VoucherDesc
# from paypal.standard.forms import PayPalPaymentsForm
# from django.urls import reverse
from .utils import id_generator





stripe_publishable_key = 'pk_test_buzoHG2AIRTPXjcScsj5iCt7'
stripe.api_key = 'sk_test_ArGeZotDEgJMqthmTOGdi7Za'


def checkout_tour(request, **kwargs):
    if request.method == "POST":
        try:
            date = datetime.datetime.strptime(request.POST['date'], '%Y-%m-%d')
        except:
            date = datetime.datetime.now()

        try:
            children = int(request.POST['children'])
        except:
            children = 0

        try:
            adults  = int(request.POST['adults'])
        except:
            adults = 1

        try:
            seniors  = int(request.POST['seniors'])
        except:
            seniors = 0

        try:
            infant = int(request.POST['enfant']) 
        except:
            infant = 0
        
        tour_detail = get_object_or_404(TourDetail, id=kwargs['tour_detail_id'])
        tour        = tour_detail.tour

        if tour.days_in_advance != '0':
            cmp_date = ( datetime.datetime.now() + datetime.timedelta(days=int(tour.days_in_advance)) )
            if date.date() < cmp_date.date():
                return single_tour(request, error=-1, tour_slug=tour_detail.slug)
        
        price_adult     = int(tour.price_adult) * adults if 'adults' in request.POST else 0
        price_children  = int(tour.price_children) * children if 'children' in request.POST else 0
        price_senior    = int(tour.price_senior) * seniors if 'seniors' in request.POST else 0
        price_infant    = int(tour.price_infant) * infant if 'infant' in request.POST else 0
        total           = int(price_adult) + int(price_children) + int(price_senior) + int(price_infant)


        # paypal_dict = {
        #         "business": "slimaidarismail@gmail.com",
        #         "amount": total,
        #         "item_name": tour_detail.title,
        #         'quantity': "x{} seniors, x{} adults, x{}children, {}infants".format(seniors,adults,children,infant),
        #         'currency_code': 'EUR',
        #         "invoice": id_generator(),
        #         "notify_url": request.build_absolute_uri(reverse('checkout:checkout-tours', args=[tour_detail.id])),
        #         "return": request.build_absolute_uri(reverse('checkout:checkout-tours', args=[tour_detail.id])),
        #         "cancel_return": request.build_absolute_uri(reverse('checkout:checkout-tours', args=[tour_detail.id])),
        #         "custom": "premium_plan",  # Custom command to correlate to some function later (optional)
        #     }

        # pay_form = PayPalPaymentsForm(initial=paypal_dict)

        context = {
            'tour': tour,
            'tour_detail': tour_detail,
            'total': total,
            'adults': adults,
            'children': children,
            'seniors': seniors,
            'infant': infant,
            'date': date,
            'form': BillingForm(),
            # 'pay_form': pay_form,
            'unique_id': id_generator()
        }

        return render(request, 'checkout/checkout-home.html', context)
    else:
        return redirect('/')

def save_bill(request, **kwargs):
    if request.method == 'POST':
        form = BillingForm(request.POST)
        if form.is_valid():
            amount  = int(kwargs['total'])*100
            tour    = get_object_or_404(Tour, id=kwargs['tour_id'])

            bill                = form.save()
            bill.amount         = amount
            bill.product        = tour
            bill.nb_infant      = kwargs['nb_infant']
            bill.nb_children    = kwargs['nb_children']
            bill.nb_adults      = kwargs['nb_adults']
            bill.nb_seniors     = kwargs['nb_senior']
            bill.date_book      = kwargs['date']
            bill.save()

            voucher      = Voucher.objects.filter(bill=bill).first()
            tour_detail  = get_object_or_404(TourDetail, id=kwargs['tour_id'])
            desc         = VoucherDesc.objects.filter(type=tour.category).first()
            voucher.desc = desc
            voucher.save()

            customer = stripe.Customer.create(
            email= bill.email,
            source=request.POST['stripeToken']
            )
            charge = stripe.Charge.create(
                customer    = customer.id,
                amount      = int(bill.amount),
                currency    = 'eur',
                description = kwargs['tour']
            )

            send_mail(
                    'Subject here',
                    'Here is the message.',
                    settings.EMAIL_HOST,
                    [bill.email],
                    fail_silently=False,
                )
            
            context = {
            'billing'    : Billing,
            'tour'       : tour,
            'voucher'    : voucher,
            'children'   : kwargs['nb_children'] if 'nb_children' in kwargs else 0,
            'adults'     : kwargs['nb_adults'] if 'nb_adults' in kwargs else 0,
            'seniors'    : kwargs['nb_senior'] if 'nb_senior' in kwargs else 0,
            'infant'     : kwargs['nb_infant'] if 'nb_infant' in kwargs else 0,
            'date'       : kwargs['date'],
            }
            return render(request, 'checkout/confirmation.html',context)
        else:
            return redirect('/')
