from django.urls import path
from . views import checkout_tour, save_bill

app_name = 'checkout'

urlpatterns = [
    path('<int:tour_detail_id>', checkout_tour, name='checkout-tours'),
    path('bill/<str:total>/<str:tour>/<int:nb_adults>/<int:nb_children>/<int:nb_senior>/<int:nb_infant>/<int:tour_id>/<str:date>', save_bill, name='save-bill'),
    # path('charge/<str:lang>/<str:total>/<str:tour>/<int:nb_adults>/<int:nb_children>/<int:nb_senior>/<int:nb_infant>/<int:tour_id>/<str:date>', charge, name='charge'),
]
