from django import forms
from .models import Billing
from django_countries.fields import CountryField
from checkout.models import Billing

class BillingForm(forms.ModelForm):
    class Meta:
        model = Billing
        fields = ('first_name', 'last_name', 'email', 'telephone', 'country')
    first_name  = forms.CharField(widget=(forms.TextInput(
        attrs={
            'class': 'form-control'
        }
    )))
    last_name   = forms.CharField(widget=(forms.TextInput(
        attrs={
            'class': 'form-control'
        }
    )))
    email       = forms.CharField(widget=(forms.EmailInput(
        attrs={
            'class': 'form-control'
        }
    )))
    telephone       = forms.CharField(widget=(forms.TextInput(
        attrs={
            'class': 'form-control'
        }
    )))