from django.db import models
from .utils import id_generator
from django.db.models.signals import  post_save
from tours.models import Tour, CATEGORIES
from django_countries.fields import CountryField



blling_status_choices = (
    ('delivered','Delivered'),
    ('pending','Pending'),
    ('refund','Refund'),
)
class Billing(models.Model):
    first_name = models.CharField(max_length=200)
    last_name  = models.CharField(max_length=200)
    email      = models.EmailField()
    telephone  = models.CharField(max_length=200)
    country    = CountryField()
    amount     = models.CharField(max_length=30, blank=True, null=True)
    status     = models.CharField(max_length=200, choices=blling_status_choices, blank=True, null=True)
    # description= models.CharField(max_length=300, blank=True, null=True)
    product    = models.ForeignKey(Tour, on_delete=models.SET_NULL, blank=True, null=True)
    nb_infant  = models.IntegerField(blank=True, null=True)
    nb_children= models.IntegerField(blank=True, null=True)
    nb_adults  = models.IntegerField(blank=True, null=True)
    nb_seniors = models.IntegerField(blank=True, null=True)
    date_order = models.DateField(auto_now_add=True) #imta dar l booking
    date_book = models.DateTimeField(blank=True, null=True) #imta bgha idir tour

    def __str__(self):
        return "{} {}".format(self.first_name, self.last_name)

class VoucherDesc(models.Model):
    type        =  models.CharField(max_length=20,choices=CATEGORIES)
    how_to_use  = models.TextField()


class Voucher(models.Model):
    No        = models.CharField(max_length=200, unique=True)
    bill      = models.OneToOneField(Billing, on_delete=models.SET_NULL, blank=True, null=True)
    desc      = models.ForeignKey(VoucherDesc, blank=True, null=True, on_delete=models.SET_NULL)
    datestamp = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.No

def billing_pre_save(sender, instance, created, *args, **kwargs):
    if created:
        No = id_generator()
        voucher = Voucher.objects.filter(No=No)
        while voucher.exists():
            No = id_generator()
        new_voucher = Voucher(
            No = No,
            bill= instance,
        )
        new_voucher.save()
post_save.connect(billing_pre_save, sender=Billing)
