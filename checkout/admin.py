from django.contrib import admin
from django_summernote.admin import SummernoteModelAdmin

from checkout.models import VoucherDesc, Voucher, Billing


# Register your models here.

class VoucherAdmin(SummernoteModelAdmin):
    summernote_fields = ('how_to_use',)

admin.site.register(VoucherDesc, VoucherAdmin)
admin.site.register(Voucher)
admin.site.register(Billing)