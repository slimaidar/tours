from django.contrib.auth import login
from django.http import HttpResponseRedirect
from django.shortcuts import render
from .forms import RegisterUserForm

def register(request):
    form = RegisterUserForm()
    if request.method == 'POST':
        form = RegisterUserForm(request.POST)
        if form.is_valid():
            user = form.save()
            login(request, user)
            return HttpResponseRedirect('/')
    return render(request, 'registration/register.html', {'form':form})


