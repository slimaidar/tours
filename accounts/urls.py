from django.contrib.auth.views import LoginView, LogoutView
from django.urls import path
from accounts.views import register
from django.contrib.auth.views import  (login, 
                                    logout,     
                                    password_reset, 
                                    password_reset_done,
                                    password_reset_confirm, 
                                )
app_name = "accounts"


urlpatterns = [
    path('login/', LoginView.as_view(), name='login'),
    path('logout/', LogoutView.as_view(), name='logout'),
    path('register/', register, name='register'),
    # path('password_reset/', password_reset, name='password_reset'),
    # path('password_reset/done/', password_reset_done, name='password_reset_done'),
]
