from django.urls import path, include
from .views import about, contact, NewsLetterViewSet
from rest_framework import routers
from django.contrib.auth.decorators import login_required

router = routers.DefaultRouter()
router.register('site_info', NewsLetterViewSet)

app_name = 'site_info'

urlpatterns = [
    path('about', about, name='about-page'),
    path('contact', contact, name='contact-page'),
    path('api/newsletter/', include(router.urls))
]