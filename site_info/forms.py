from django import forms


class ContactUsForm(forms.Form):
    email = forms.EmailField()
    subject = forms.CharField(max_length=200)
    message = forms.TextInput()