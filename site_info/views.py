from django.core.mail import send_mail
from django.shortcuts import render

from site_info.forms import ContactUsForm
from src import settings
from .models import NewsLetter
from rest_framework import viewsets
from .serializers import NewsLetterSerializer


def about(request, *args, **kwargs):
    return render(request, 'site_info/about.html', {"about": 'about'})


def contact(request):
    # form = ContactUsForm()
    if request.method == 'POST':
        try:
            message = 'From {}\n message:\n {}'.format(request.POST['email'], request.POST['message'])
            send_mail(
                request.POST['subject'],
                message,
                settings.EMAIL_HOST,
                ['ismailaidar@gmail.com'],
                fail_silently=False,
            )
            return render(request, 'site_info/contact.html', {"success": 1})
        except:
            return render(request, 'site_info/contact.html', {"success": 0})

    return render(request, 'site_info/contact.html')


class NewsLetterViewSet(viewsets.ModelViewSet):
    queryset = NewsLetter.objects.all()
    serializer_class = NewsLetterSerializer
