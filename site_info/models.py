from django.db import models
from tours.models import Language

# Create your models here.
class Slider(models.Model):
    image = models.ImageField(upload_to="img/slides")
    title1 = models.CharField(max_length=200)
    title2 = models.CharField(max_length=200)

    def __str__(self):
        return self.title1

class Contact(models.Model):
    phone = models.CharField(max_length=200)
    mail  = models.EmailField()

about_choices = (
    ('about', 'about'),
    ('terms', 'terms'),
)
class About(models.Model):
    language    = models.ForeignKey(Language, on_delete=models.SET_NULL, blank=True, null=True)
    text        = models.TextField()
    page        = models.CharField(choices=about_choices, null=True, max_length=200)

    def __str__(self):
        return "About in -{}-".format(self.language)


class NewsLetter(models.Model):
    email = models.EmailField(unique=True)

    def __str__(self):
        return self.email