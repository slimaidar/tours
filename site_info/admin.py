from django.contrib import admin
from django_summernote.admin import SummernoteModelAdmin

from .models import Slider, About


# Register your models here.
admin.site.site_header = 'MarrakeshCityTour Administrations'
admin.site.site_title = 'MarrakeshCityTour Administrations'

class AboutAdmin(SummernoteModelAdmin):
    summernote_fields = ('text',)

admin.site.register(Slider)
admin.site.register(About, AboutAdmin)