from django.utils.text import slugify
import random
import string
import requests
from bs4 import BeautifulSoup as soup


def random_string_generator(size=10, chars=string.ascii_lowercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))


def unique_slug_generator(instance, new_slug=None):
    if new_slug is not None:
        slug = new_slug
    else:
        slug = slugify(instance.title)

    Klass = instance.__class__
    qs_exists = Klass.objects.filter(slug=slug).exists()
    if qs_exists:
        new_slug = "{slug}-{randstr}".format(
            slug=slug,
            randstr=random_string_generator(size=4)
        )
        return unique_slug_generator(instance, new_slug=new_slug)
    return slug

def change_currency(currency):
    page = requests.get('https://www.xe.com/currencyconverter/convert/?Amount=1&From=EUR&To='+str(currency))
    my_soup = soup(page.text, 'html.parser')
    price = my_soup.find(class_='uccResultAmount')
    return float(price.text)