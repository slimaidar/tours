from django import template
import math
from tours.utils import change_currency
import decimal


register = template.Library()

@register.filter
def multiply(value, arg):
    try:
        result = float(value) * float(arg)
    except:
        return None
    return math.ceil(decimal.Decimal(result))