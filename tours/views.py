from django.conf import settings
from django.core.paginator import Paginator
from django.shortcuts import redirect, render, HttpResponse
from .utils import change_currency
from .models import Language, Tour, TourDetail, City
from tours.models import TourDetail
from site_info.models import Slider
from django.shortcuts import  get_object_or_404
from reviews.forms import ReviewForm
from reviews.models import Review


# Create your views here.
langs = settings.LANGUAGES

def change_currency_post(request):
    currency = 1
    if request.method == 'POST':
        currency = request.POST.get('currency')
        request.session['currency'] = change_currency(currency)
        if currency == 'USD':
            request.session['currency_short'] = '$'
        elif currency == 'EUR':
            request.session['currency_short'] = '€'
        else:
            request.session['currency_short'] = currency
    else:
        if 'currency' not in request.session:
            request.session['currency'] = '1'
            request.session['currency_short'] = '€'


def home_page(request, *args, **kwargs):
    change_currency_post(request)

    # lang = Language.objects.filter(title=request.LANGUAGE_CODE).first()
    lang = get_object_or_404(Language, title=request.LANGUAGE_CODE)

    feautured_tours = Tour.objects.filter(feautured=True)
    active_tours = feautured_tours.filter(active=True)
    active_exps = feautured_tours.filter(active=True)

    active_tours = active_tours.filter(category='tour')
    active_experiences = active_exps.filter(category='experience')

    tours = TourDetail.objects.filter(Language_id=lang.id).filter(
        tour__in=active_tours).order_by('-id')[:6]
    experiences = TourDetail.objects.filter(Language_id=lang.id).filter(
        tour__in=active_experiences).order_by('-id')[:6]

    context = {
        'tours': tours,
        'experiences': experiences
    }
    return render(request, 'tours/home.html', context)


def single_tour(request,error=None, **kwargs):
    change_currency_post(request)
    error = error
    tour = get_object_or_404(TourDetail, slug=kwargs['tour_slug'])
    av = []
    for item in tuple(tour.tour.availability):
        av.append(item)
    review_form = ReviewForm()
    reviews = Review.objects.filter(tour=tour.tour).order_by('-id')[:6]
    context = {
        'tour': tour,
        'availability': av,
        'review_form' : review_form,
        'reviews': reviews,
        'error': error
    }
    return render(request, 'tours/single_tour.html', context)


def tours_list(request):
    change_currency_post(request)
    tours        = Tour.objects.all()
    lang         = get_object_or_404(Language, title=request.LANGUAGE_CODE)
    tours_detail = TourDetail.objects.filter(Language=lang)

    if 'type' in request.GET:
        tours = tours.filter(category=request.GET['type']) if request.GET['type'] != 'all' else tours
    if 'city' in request.GET:
        city = get_object_or_404(City, name=request.GET['city']) if request.GET['city'] != 'all' else tours_detail
        tours_detail = tours_detail.filter(city=city) if request.GET['city'] != 'all' else tours_detail

    tours_detail = tours_detail.filter(tour__in=tours)

    paginator = Paginator(tours_detail, 15)
    if request.GET.get('page'):
        page = request.GET.get('page')
    else:
        page = 1
    f_tours = paginator.get_page(page)
    
    context = {
        'tours': f_tours
    }
    return render(request, 'tours/tours_list.html', context)