from django.db import models
from .utils import unique_slug_generator
from django.db.models.signals import pre_save
from multiselectfield import MultiSelectField


class Language(models.Model):
    title = models.CharField(max_length=3, unique=True)
    image = models.ImageField(upload_to='languages')

    def __str__(self):
        return self.title


class City(models.Model):
    name        = models.CharField(max_length=200)
    description = models.TextField(blank=True, null=True)
    image       = models.ImageField(blank=True, null=True)

    def __str__(self):
        return self.name

ALLOWED_ITEMS = (
    ('pets','Pets Allowed'),
    ('tour_guide','Tour Guide'),
    ('accessibiliy','Accessibiliy'),
    ('audio_guide','Audio guide'),
    ('none','None of the records (if you check this one all of choices above will be ignored)'),
)
ALLOWED_AGES = (
    ('seniors','Seniors'),
    ('adults','Adults'),
    ('children','Children'),
    ('enfant','Enfant'),
)
ALLOWED_DAYS = (
    ('0','Sunday'),
    ('1','Monday'),
    ('2','Tuesday'),
    ('3','Wednesday'),
    ('4','Thursday'),
    ('5','Friday'),
    ('6','Saturday'),
    ('7', 'Daily'),
)
TOUR_TAGS = (
    ('none','NONE'),
    ('POPULAR','POPULAR'),
    ('SELLS OUT FAST','SELLS OUT FAST'),
    ('NEW','NEW'),
    ('BEST SELLER','BEST SELLER'),
)
DAYS_IN_ADVANCED = (
    ('0','None'),
)
CATEGORIES = (
    ('tour','Tour'),
    ('experience','Experience'),
)
for i in range (1,31):
    DAYS_IN_ADVANCED+= (("{}".format(i),"{}".format(i)),)
class Tour(models.Model):
    title               = models.CharField(max_length=200, help_text="this title is only to help you know wihch tour is this")
    from_date           = models.DateField(blank=True, null=True)
    to_date             = models.DateField(blank=True, null=True)
    from_time           = models.TimeField(blank=True, null=True)
    to_time             = models.TimeField(blank=True, null=True)
    availability        = MultiSelectField(choices=ALLOWED_DAYS)
    allowed_items       = MultiSelectField(choices=ALLOWED_ITEMS, blank=True, null=True)
    allowed_ages        = MultiSelectField(choices=ALLOWED_AGES)

    age_senior          = models.CharField(max_length=100,blank=True, null=True, help_text="Eg: 90+")
    price_senior        = models.IntegerField(blank=True, null=True, default=0)
    age_adult           = models.CharField(max_length=100,blank=True, null=True, help_text="Eg: 14-50")
    price_adult         = models.IntegerField(blank=True, null=True, default=0)
    age_children        = models.CharField(max_length=100,blank=True, null=True, help_text="Eg: 14-50")
    price_children      = models.IntegerField(blank=True, null=True, default=0)
    age_infant          = models.CharField(max_length=100,blank=True, null=True, help_text="Eg: 14-50")
    price_infant        = models.IntegerField(blank=True, null=True, default=0)

    hours               = models.CharField(max_length=100,blank=True, null=True, help_text="Leave it empty if it is not availble on the tour")
    minutes             = models.CharField(max_length=100,blank=True, null=True, help_text="Leave it empty if it is not availble on the tour")
    days                = models.CharField(max_length=100,blank=True, null=True, help_text="Leave it empty if it is not availble on the tour")

    days_in_advance     = models.CharField(max_length=20,choices=DAYS_IN_ADVANCED)
    languages           = models.ManyToManyField(Language, blank=True)
    category            = models.CharField(max_length=20,choices=CATEGORIES)
    tag                 = models.CharField(max_length=200, choices=TOUR_TAGS)
    image               = models.ImageField(upload_to='image/tours')
    image1              = models.ImageField(upload_to='image/tours')
    image2              = models.ImageField(upload_to='image/tours')
    image3              = models.ImageField(upload_to='image/tours')
    wallpaper           = models.ImageField(upload_to='image/tours', blank=True, null=True)
    active              = models.BooleanField(default=True)
    feautured           = models.BooleanField(default=False, help_text="check this one if you want it to be displayed at home page")

    def __str__(self):
        return str(self.title)


class TourGallery(models.Model):
    tour = models.ForeignKey(Tour, on_delete=models.CASCADE)
    image = models.ImageField(upload_to="img/tours_gallery")

    def __str__(self):
        return "Image for {}".format(self.tour.title)

class TourDetail(models.Model):
    tour        = models.ForeignKey(
                    Tour, on_delete=models.SET_NULL, blank=True, null=True)
    Language    = models.ForeignKey(
                    Language, on_delete=models.SET_NULL, blank=True, null=True)
    title       = models.CharField(max_length=200)
    city        = models.ForeignKey(
                    City, on_delete=models.SET_NULL, blank=True, null=True)
    description = models.TextField()
    summary     = models.TextField()
    included    = models.TextField()
    excluded    = models.TextField()
    slug        = models.SlugField(unique=True, blank=True, null=True, help_text="leave it empty if you don't have a good keyword in your head")

    def __str__(self):
        return self.title



def tourDetail_pre_save(sender, instance, *args, **kwargs):
    if not instance.slug:
        instance.slug = unique_slug_generator(instance)

pre_save.connect(tourDetail_pre_save, sender=TourDetail)
