from django.urls import path, include
from .views import home_page, single_tour, tours_list

app_name = 'tours'

urlpatterns = [
    path('', home_page, name='home-page'),
    path('tours/<str:tour_slug>', single_tour , name='single_tour'),
    path('all_tours_experiences/', tours_list , name='tours_list'),
]