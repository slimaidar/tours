from django.contrib import admin
from django_summernote.admin import SummernoteModelAdmin

from .models import Language, Tour, TourDetail, City, TourGallery


# Register your models here.


class TourDetailAdmin(SummernoteModelAdmin):
    list_display = ['__str__', 'tour', 'Language', 'slug']
    summernote_fields = ('description','included', 'excluded',)

class TourAdmin(admin.ModelAdmin):
    list_display = ['__str__', 'price_adult']

class CityAdmin(admin.ModelAdmin):
    summernote_fields = ('description',)

admin.site.register(Language)
admin.site.register(Tour, TourAdmin)
admin.site.register(TourDetail, TourDetailAdmin)
admin.site.register(City, CityAdmin)
admin.site.register(TourGallery)
