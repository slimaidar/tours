from django import forms
from .models import Review
from checkout.models import Voucher

class ReviewForm(forms.ModelForm):
    class Meta:
        model = Review
        fields = ['text', 'voucher']
    text = forms.CharField(widget=forms.Textarea(
        attrs={
            'class': 'form-control',
            'rows':"8",
            "cols":"90"
        }
    ))
    voucher = forms.CharField(widget=forms.TextInput(
        attrs={
            'class': 'form-control',
            'placeholder': 'Voucher number'
        }
    ))

    def clean_voucher(self):
        try:
            voucher = Voucher.objects.get(No=self.cleaned_data['voucher'])
            return voucher
        except:
            raise forms.ValidationError("Voucher code is not valid")
