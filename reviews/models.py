from django.db import models
from checkout.models import Voucher
from tours.models import Tour


class Review(models.Model):
    rate    = models.IntegerField(blank=True, null=True)
    text    = models.TextField()
    tour    = models.ForeignKey(Tour, on_delete=models.CASCADE, blank=True, null=True)
    voucher = models.ForeignKey(Voucher, on_delete=models.CASCADE, blank=True, null=True)
    timespan= models.DateTimeField(auto_now_add=True)