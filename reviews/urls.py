from django.urls import path
from .views import add_review

app_name = "reviews"


urlpatterns = [
    path('/<int:tour_id>/<str:tour_title>', add_review, name='add_review'),
    path('/<int:tour_slug>', add_review, name='all_reviews'),
]
