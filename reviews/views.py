from django.shortcuts import render
from .forms import ReviewForm
from .models import Review
from tours.models import Tour, TourDetail
from django.shortcuts import redirect
from checkout.models import Voucher
from tours.views import single_tour
from django.shortcuts import get_object_or_404

# Create your views here.
def add_review(request, tour_id, tour_title):
    tour = get_object_or_404(Tour, id=tour_id)
    detail = get_object_or_404(TourDetail, slug=tour_title)
    all_reviews = Review.objects.filter(tour=tour)
    if request.method == 'POST':
        form = ReviewForm(request.POST)
        if form.is_valid():
            print('here')

            reviews = Review.objects.filter(voucher=form.cleaned_data['voucher']).filter(tour=tour).count()
            all_reviews = Review.objects.filter(tour=tour)
            if reviews >= 3:
                return render(request, 'reviews/all_reviews.html', {'reviews': all_reviews, 'msg': '-1', 'tour': tour, 'detail': detail})
            else:
                try:
                    tour = Tour.objects.get(id=request.POST['tour'])
                except:
                    return render(request, 'reviews/all_reviews.html', {'reviews': all_reviews, 'msg': '-2', 'tour': tour, 'detail': detail})
                review = form.save()
                review.tour = tour
                try:
                    review.rate = int(request.POST['rate'])
                except:
                    return render(request, 'reviews/all_reviews.html', {'reviews': all_reviews, 'msg': '-3', 'tour': tour, 'detail': detail})
               
                review.save()
                return render(request, 'reviews/all_reviews.html', {'reviews': all_reviews, 'msg': 1, 'tour': tour, 'detail': detail})
        else:
            return render(request, 'reviews/all_reviews.html', {'reviews': all_reviews, 'msg': '-2', 'tour': tour, 'detail': detail})
    else:
        all_reviews = Review.objects.filter(tour=tour)
        return render(request, 'reviews/all_reviews.html', {'reviews': all_reviews, 'tour': tour, 'detail': detail})

def all_review(request):
    pass
