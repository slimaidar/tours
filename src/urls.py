from django.contrib import admin
from django.urls import path, include
from django.contrib.auth.decorators import login_required
from django.conf import settings
from django.conf.urls.static import static
from django.conf.urls.i18n import i18n_patterns


urlpatterns = [
    path('admin/', admin.site.urls),
    path('myadmintours/', include('myadmin.urls', namespace='myadmin')),
    path('summernote/', include('django_summernote.urls')),
    path('paypal/', include('paypal.standard.ipn.urls')),
]
urlpatterns += i18n_patterns(
    path('accounts/', include("accounts.urls", namespace="accounts")),
    path('', include('tours.urls', namespace='tours')),
    path('inf/', include('site_info.urls', namespace='site_info')),
    path('checkout/', include('checkout.urls', namespace='checkout')),
    path('reviews/', include('reviews.urls', namespace='reviews')),
)

urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
