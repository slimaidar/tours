from django.urls import  path
from django.contrib.auth.decorators import login_required
from .views import (home, add_tour,
                    add_detail, tours_list, show_detail_page,
                    update_tour, delete_tour, tour_details, tour_detail_delete,
                    tour_detail_update, check_voucher, all_orders,languages_view,
                    add_language, delete_language)


app_name = 'myadmin'

urlpatterns = [
    path('', login_required(home) , name='myadmin-home'),
    path('add-tour', login_required(add_tour) , name='myadmin-add-tour'),
    path('update-tour/<int:id>', login_required(update_tour) , name='myadmin-update-tour'),
    path('delete-tour/<int:id>', login_required(delete_tour) , name='myadmin-delete-tour'),
    path('add-detail', login_required(add_detail) , name='myadmin-add-detail'),
    path('tours-list', login_required(tours_list) , name='myadmin-tours-list'),
    path('detail-form/<int:tour>', login_required(show_detail_page) , name='myadmin-detail-form'), # made this only to send tour from tours list
    path('tour-details/<int:tour>', login_required(tour_details) , name='myadmin-tour-details'),
    path('tour-details/delete/<int:detail>', login_required(tour_detail_delete) , name='myadmin-detail-delete'),
    path('tour-details/update/<int:detail>', login_required(tour_detail_update) , name='myadmin-detail-update'),
    path('check-voucher', login_required(check_voucher), name='check-voucher'),
    path('all-orders', login_required(all_orders), name='all-orders'),
    path('languages', login_required(languages_view), name='languages'),
    path('languages/add', login_required(add_language), name='add-language'),
    path('languages/update/<int:id>', login_required(add_language), name='update-language'),
    path('languages/delete/<int:id>', login_required(delete_language), name='delete-language'),
]
