from django import forms
from django_summernote.widgets import SummernoteWidget, SummernoteInplaceWidget
from tours.models import Tour, TourDetail, Language

class TourForm(forms.ModelForm):
    class Meta:
        model   = Tour
        fields  = ('title', 'from_time', 'to_time', 'availability', 'allowed_ages', 'age_senior', 'price_senior', 'age_adult', 'price_adult',
                    'age_children', 'price_children', 'age_infant', 'price_infant', 'hours', 'minutes', 'days', 'days_in_advance', 'languages', 'category',
                    'tag', 'image', 'image1', 'image2', 'image3', 'wallpaper', 'active', 'feautured')
    title = forms.CharField(widget=forms.TextInput(
        attrs={
            'class': 'form-control',
            'id': 'title'
        }
    ))
    # from_date = forms.DateField(widget=forms.DateInput(
    #     attrs = {
    #         'class': 'form-control',
    #         'id': 'from_date',
    #         'type': 'date',
    #     }
    # ))
    # to_date = forms.DateField(widget=forms.DateInput(
    #     attrs = {
    #         'class': 'form-control',
    #         'id': 'from_date',
    #         'type': 'date'
    #     }
    # ))
    from_time = forms.TimeField(widget=forms.TimeInput(
        attrs = {
            'class': 'form-control',
            'id': 'from_time',
            'type': 'time'
        }
    ))
    to_time = forms.TimeField(widget=forms.TimeInput(
        attrs = {
            'class': 'form-control',
            'id': 'from_time',
            'type': 'time'
        }
    ))
    hours = forms.CharField(widget=forms.NumberInput(attrs={
        'class': 'form-control'
    }))
    minutes = forms.CharField(widget=forms.NumberInput(attrs={
        'class': 'form-control'
    }))
    days = forms.CharField(widget=forms.NumberInput(attrs={
        'class': 'form-control'
    }))
    DAYS_IN_ADVANCED = (
        ('0','NONE'),
    )
    for i in range (1,31):
        DAYS_IN_ADVANCED+= (("{}".format(i),"{}".format(i)),)
    days_in_advance = forms.ChoiceField(widget=forms.Select(attrs={'class': 'form-control'}),
    choices=(DAYS_IN_ADVANCED))


class DetailsForm(forms.ModelForm):
    class Meta:
        model  = TourDetail
        fields = ['Language', 'title', 'city', 'description', 'summary', 'included', 'excluded']
        widgets = {
            'description':  SummernoteWidget(),
            'included':     SummernoteWidget(),
            'excluded':     SummernoteWidget(),
        }


class CheckVoucherForm(forms.Form):
    voucher_no = forms.CharField(max_length=100, widget=forms.TextInput(
    attrs={
        'class': 'form-control'
    }
    ))

    def clean_voucher_no(self):
        no = self.cleaned_data['voucher_no']
        if "'" in no or "(" in no or ")" in no or '"' in no:
            raise ValidationError("Use only letters and numbers.")
        else:
            return no


class LanguageForm(forms.ModelForm):
    class Meta:
        model = Language
        fields = ['title', 'image']
        