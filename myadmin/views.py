from django.shortcuts import render, redirect
from checkout.models import Billing
import datetime
from django.db.models import Sum
from .forms import TourForm, DetailsForm, CheckVoucherForm, LanguageForm
from tours.models import Tour, TourDetail, Language
from checkout.models import Billing
from checkout.models import Voucher
from django.shortcuts import  get_object_or_404
import datetime


def home(request):
    orders = Billing.objects.order_by('-id')[:30]
    orders_today = Billing.objects.filter(date_order=datetime.datetime.now().date())
    today_income = orders_today.aggregate(Sum('amount'))
    total_orders = Billing.objects.all()
    total_income = total_orders.aggregate(Sum('amount'))
    context = {
        'orders': orders,
        'orders_today': orders_today,
        'today_income': today_income,
        'total_orders': total_orders,
        'total_income': total_income
    }
    return render(request, 'myadmin/home.html', context)


def add_tour(request, *args, **kwargs):
    form = TourForm()
    if request.method == 'POST':
        form = TourForm(request.POST, request.FILES)
        if form.is_valid():
            tour = form.save()
            if request.POST['action'] == 'next':
                tour = get_object_or_404(Tour, id=tour.id)
                form_detail = DetailsForm()
                return render(request, 'myadmin/tours/adddetails.html', {'tour': tour, 'form': form_detail})
            else:
                tours = Tour.objects.order_by('-id')
                return render(request, 'myadmin/tours/all-tours.html', {'added': True, "tours": tours})
    context = {
        'form': form
    }
    return render(request, 'myadmin/tours/add.html', context)


def update_tour(request, id):
    tour = get_object_or_404(Tour, id=id)
    if request.method == 'POST':
        form = TourForm(request.POST or None, request.FILES, instance=tour)
    else:
        form = TourForm(request.POST or None, instance=tour)
    if form.is_valid():
        form.save()
    return render(request, 'myadmin/tours/add.html', {"form": form, "update": True, "tour": tour})


def delete_tour(request, id):
    if request.method == 'POST':
        tour = get_object_or_404(Tour, id=id)
        tour.delete()
        tours = Tour.objects.order_by('-id')
        return render(request, 'myadmin/tours/all-tours.html', {'deleted': True, "tours": tours})
    return render(request, 'myadmin/tours/delete_tour.html')


def show_detail_page(request, tour):
    tour = get_object_or_404(Tour, id=tour)
    form_detail = DetailsForm()
    return render(request, 'myadmin/tours/adddetails.html', {'tour': tour, 'form': form_detail})


def add_detail(request):
    if request.method == 'POST':
        form = DetailsForm(request.POST)
        tour = get_object_or_404(Tour, id=request.POST['tour'])
        if form.is_valid():
            detail = form.save()
            detail.tour = tour
            detail.save()
            form = DetailsForm()
            return render(request, 'myadmin/tours/adddetails.html', {'form': form, 'success': True})
    else:
        return redirect("myadmin:myadmin-home")


def tours_list(request):
    tours = Tour.objects.order_by('-id')
    return render(request, 'myadmin/tours/all-tours.html', {'tours': tours})


def tour_details(request, tour):
    tour = Tour.objects.get(id=tour)
    details = TourDetail.objects.filter(tour=tour)
    context = {
        'tour' : tour,
        'details': details
    }
    return render(request, 'myadmin/details/tour-details.html', context)


def tour_detail_delete(request, detail):
    if request.method == 'POST':
        tour_detail = get_object_or_404(TourDetail, id=detail)
        tour_detail.delete()
        tours = Tour.objects.order_by('-id')
        return render(request, 'myadmin/tours/all-tours.html', {'deleted': True, "tours": tours})
    return render(request, 'myadmin/tours/delete_tour.html')


def tour_detail_update(request, detail):
    updated = "0"
    detail = get_object_or_404(TourDetail, id=detail)
    form = DetailsForm(request.POST or None, instance=detail)
    if form.is_valid():
        form.save()
        updated = "1"
    return render(request, 'myadmin/tours/adddetails.html', {"form": form, "updated": updated, "detail": detail})


def check_voucher(request):
    form = CheckVoucherForm()
    if request.method == 'POST':
        form = CheckVoucherForm(request.POST)
        if form.is_valid():
            no = request.POST['voucher_no']
            exists = '-'
            voucher = None
            try:
                voucher = Voucher.objects.get(No=no)
                exists = True
            except Voucher.DoesNotExist:
                exists = False

            context = {
                'form': form,
                'exists': exists,
                'voucher': voucher
            }

            return render(request, 'myadmin/voucher/check_voucher.html', context)

    return render(request, 'myadmin/voucher/check_voucher.html', {'form': form})


def all_orders(request):
    orders = Billing.objects.order_by('-id')
    context = {
        'orders':orders
    }
    return render(request, 'myadmin/orders/all_orders.html', context)


def languages_view(request):
    languages = Language.objects.all()
    context = {
        'languages': languages
    }
    return render(request, 'myadmin/language/home.html', context)

def add_language(request, *args, **kwargs):
    form = LanguageForm()
    if 'id' in kwargs:
        language = get_object_or_404(Language, id=kwargs['id'])
        form = LanguageForm(request.POST or None, instance=language)
        if request.method == 'POST':
            form = LanguageForm(request.POST, request.FILES, instance=language)
            if form.is_valid():
                form.save()
                return render(request, 'myadmin/language/home.html', {'languages': Language.objects.all()})
    if request.method == 'POST':
        form = LanguageForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            return redirect('/myadmintours/languages')
    context = {
        'form': form
    }
    return render(request, 'myadmin/language/add.html', context)

def delete_language(request, id):
    language = get_object_or_404(Language, id=id)
    language.delete()
    return redirect('/myadmintours/languages')
